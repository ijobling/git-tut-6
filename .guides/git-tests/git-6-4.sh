#!/bin/bash
QCOUNT=2

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create the requested file in your file tree" "file-4.txt" "ls /home/codio/workspace"
				;;
			2 )
				expect "Commit the new file with the requested message" "4th[[:space:]]*commit[[:space:]]*message" "git log -4"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command