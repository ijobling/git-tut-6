#!/bin/bash
QCOUNT=4

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Reset the project 3 commits from HEAD" "1st[[:space:]]*commit[[:space:]]*message" "git log -1"
				;;
			2 )
				expect "git status does not match reference" "new file:[[:space:]]*file-2.txt" "git status"
				;;
      3 )
				expect "git status does not match reference" "new file:[[:space:]]*file-3.txt" "git status"
				;;
      4 )
				expect "git status does not match reference" "new file:[[:space:]]*file-4.txt" "git status"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command