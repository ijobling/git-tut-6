`git reset` can be very _destructive_ but also very helpful as it helps you keep your commit history clean. 

Remember that commit messages should not be random strings but references to what you and your team have done to a specific file or project version. This is where `git reset`, `git checkout` and `git revert` come very handy. 

Learn how to use `git revert` in the next unit. 