In the previous unit, you became familiar with the `git checkout <commit>` command that allows you undo changes in a file. Meaning that you can go back and forth between different files (or files) versions.

As you have experienced, the commit `HEAD` concept is present most of the time as it represents the most recent commit in the commit history. 