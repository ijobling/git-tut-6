```bash
git reset <commit>
```

This is slightly less destructive as it at least leaves all changes intact in the working directory.

Let's evaluate the result of doing a `git reset HEAD^^` (which is the same as referencing the `91011fgh` commit) in our visual representation:

![git-tuts-6_git-reset-commit](.guides/img/git-tuts-6_git-reset-commit.png)

- Resets the staging area to the specified commit
- Leaves the working directory untouched so all changes made since the specified commit are present
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted