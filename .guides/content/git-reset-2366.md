Whereas `git revert`, covered in the next unit, is a safe way to undo changes, `git reset` is the unsafe approach.

It should be used with full awareness as it is one of the only Git commands that has a destructive capability.

There are three variations for you to consider:

- `git reset --hard <commit>`
- `git reset <commit>`
- `git reset --soft <commit>`

Let's cover each one of them having as a reference the following visual representation: 

In our example, a `git log -4` will output this: 

![git-tut-6_git-log](.guides/img/git-tuts-6_git-log-4.png)

In the image above, the left side represents the commit messages on `git log -4`, the selected `*master` branch is represented by the blue arrow and each commit in the `master` branch history is represented by a blue dot where `HEAD` is the blue dot with the black stroke.

For the sake of the following examples, it is important to focus on how the files, `file-*.txt`, were added on different Git commits.

---
Go to the next next section.