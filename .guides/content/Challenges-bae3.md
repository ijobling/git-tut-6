The following set of challenges are designed to help you understand `git reset`. 

After completing the challenges, your file structure should look similar to this diagram (SHA id's may vary): 

![git-tut-6_git-log](.guides/img/git-tuts-6_git-log-4.png)

Get ready to create different commits that will represent the different versions of your project.