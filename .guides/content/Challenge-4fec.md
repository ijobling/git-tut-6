{Check It!|assessment}(test-2642806583)

Good job! Execute a `git log -4` in your terminal pane to see this (if you followed the challenges set of course):

*Commit id's, user data and dates may vary.
```bash

commit 73hdye68e16948f73c5c28b1085aa4219d66c1c7
Author: user <user@email.com>
Date:   Mon Nov 9 14:53:21 2015 +0100

    4th commit message

commit 73hdye62a98aa8ff01d027896d83dea11aae8d4a
Author: user <user@email.com>
Date:   Mon Nov 9 14:53:10 2015 +0100

    3rd commit message

commit 73hdye6ea79ff58009d49490938810277a177955
Author: user <user@email.com>
Date:   Mon Nov 9 14:52:58 2015 +0100

    2nd commit message

commit 73hdye6a613d5215f288f1836ae0b9f123418e08
Author: user <user@email.com>
Date:   Mon Nov 9 13:47:55 2015 +0100

    1st commit message

```


|||guidance

### Correct answers:

1. Create the file `file-4.txt` in the file tree
2. `git add -A` `git commit -m "4th commit message"`

|||