```bash
git reset --hard <commit>
```

This is the most destructive variation as, once you have run this, all changes since the specified commit are lost:

Let's evaluate the result of doing a `git reset --hard 91011fgh` in our visual representation:

![git-tuts-6_git-reset-hard](.guides/img/git-tuts-6_git-reset-hard.png)

- The staging area will contain all changes made since the specified commit
- The working directory will also contain all changes made since the specified commit
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted

**Important** : you should **never** use `git reset` on commits that exist in a remote repository. So, only use it for commits in your local repository that have not yet been pushed.