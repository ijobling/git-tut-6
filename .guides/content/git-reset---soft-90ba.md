```bash
git reset --soft <commit>
```

This is also slightly less destructive as it at least leaves all changes intact in the working directory *and* in the staging area.

Let's evaluate the result of doing a `git reset --soft HEAD~2` (which is the same as referencing the `91011fgh` commit or doing `HEAD^^`) in our visual representation:

![git-tuts-6_git-reset-soft](.guides/img/git-tuts-6_git-reset-soft.png)

- Leaves the staging area with all changes intact (more on this below)
- Leaves the working directory intact (all changes made since the specified commit are present)
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted

### Leaves the staging area with all changes intact

In this last visual representation, you can notice what the `git status` would display if executed after the `git reset --soft HEAD~2`. 

```bash
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   folder/file-3.txt
	new file:   folder/file-4.txt
```

The output refers to the _staging_ area and displays the files that were left in the working directory but are not part of `HEAD`, hence the need to commit them (they are already "added").

---
Now we'll look at some use cases.