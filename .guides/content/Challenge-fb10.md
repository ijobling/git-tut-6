|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file file-4.txt)

|||

{Check It!|assessment}(test-3796023615)

|||guidance

### Correct answers:

1. `git reset --soft HEAD^^^` or `git reset --soft HEAD~3` or `git reset --soft <1st commit message SHA id>` 

|||