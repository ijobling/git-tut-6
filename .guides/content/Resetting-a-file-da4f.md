You can also reset a single file : 

- `git reset <filename>`

Removes a file from the staging area but leaves the working directory unchanged.

For instance, do a `git status` and notice how files `file-2.txt`, `file-3.txt` and `file-4.txt` are in the staging area, ready to be committed.

Remove the `file-4.txt` file from the staging area by executing:

```bash
git reset file-4.txt
```

And do a `git status` afterwards.