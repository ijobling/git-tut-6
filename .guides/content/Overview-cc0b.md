## Git Reset
This section discusses one of the common ways to roll back changes you have made at the commit level.

There are 3 main ways to do this :

## git reset
This allows you to roll-back to a specified commit and delete all the commits after that. This is one of the few destructive Git operations and so you should be very careful using it as you will actually lose commits. If you are working with other developers on the same repository, you should **NEVER, EVER** use this command if you reset to a commit that was pushed to a remote repository. You will be hated for it.

## git revert
This command REVERTS OUT (undoes) the changes introduced by a specific commit. It does not REVERT TO. Using revert is the safe way of rolling back changes, as we’ll see shortly.

## git checkout
This is a quick way of looking at a specific commit but you need to be careful as this puts your repo in what is known as a ‘detached’ state. More on this later.

This unit concentrates on `git reset`.
