@annotation:tour intro
#A POETIC INTRODUCTION TO GIT

##Module 6 - Git Reset
This section discusses one of the common ways to roll back changes you have made at the commit level.

There are 3 main ways to do this :

##git reset
This allows you to roll-back to a specified commit and delete all the commits after that. This is one of the few destructive Git operations and so you should be very careful using it as you will actually lose commits. If you are working with other developers on the same repository, you should **NEVER, EVER** use this command if you reset to a commit that was pushed to a remote repository. You will be hated for it.

##git revert
This command REVERTS OUT (undoes) the changes introduced by a specific commit. It does not REVERT TO. Using revert is the safe way of rolling back changes, as we’ll see shortly.

##git checkout
This is a quick way of looking at a specific commit but you need to be careful as this puts your repo in what is known as a ‘detached’ state. More on this later.

This module concentrates on `git reset`.


@annotation:tour reset
#git reset
Whereas `git revert`, covered in the next module, is a safe way to undo changes, `git reset` is the unsafe approach.

It should be used with full awareness as it is one of the only Git commands that has a destructive capability.

There are three variations for you to consider :

In all the cases below, it is not necessary to specify a commit, in which case Git assumes the HEAD commit.

#git reset --hard <commit>
This is the most destructive variation as, once you have run this, all changes since the specified commit are lost.

- The staging area will contain all changes made since the specified commit
- The working directory will also contain all changes made since the specified commit
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted

#git reset <commit>
This is slightly less destructive as it at least leaves all changes intact in the working directory.

- Resets the staging area to the specified commit
- Leaves the working directory untouched so all changes made since the specified commit are present
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted


#git reset --soft <commit>
This is also slightly less destructive as it at least leaves all changes intact in the working directory *and* in the staging area.

- Leaves the staging area with all changes intact
- Leaves the working directory intact (all changes made since the specified commit are present)
- Resets the HEAD pointer to the specified commit and all subsequent commits are deleted


**Important** : you should **never** use `git reset` on commits that exist in a remote repository. So, only use it for commits in your local repository that have not yet been pushed.

Now we'll look at some use cases.

@annotation:tour uc-hardreset
#Use Case : hard reset
Let's say you have done a couple of commits in your local repo but you decide that you want to get rid of them and reset things back to a previous commit. 

```
git reset --hard <commit>
```

This will reset your project (working directory and staging area) to the specified commit and remove *all subsequent commits* from the repo.

To visualize what is done here, the image below shows our current state. The Commit we want to reset to is circled.

![](img/reset-state-a.png)

If we were to run `git reset --hard HEAD^^` we would end up with the following state. Note how Commit 2 and Commit 3 have been completely removed.

![](img/reset-state-b.png)

Remember - only ever run this on local commits and never on commits that have been pushed to a remote repository by you or anyone else.

@annotation:tour uc-commitreset
#Use Case : reset but keep working directory  
Let's say you have a few commits that you want to combine into a single, new commit. By not using the `--hard` parameter, you can :

- remove all changes from the staging area after the specified commit
- keep all changes after the specified commit in the working directory
- remove all commits after the specified commit
- run `git add` to restage everything in the working directory (which includes all the changes in the subsequent commits, remember)
- run `git commit` to commit everything

This effectively 'squashes' all commits into a single commit. 

Remember, only ever do this for commits that you have done locally. Never for commits that have been pushed to a remote repo.

```
git reset <commit>
git add -A
git commit -m 'your commit message'
```
You could also use the `--soft` parameter and use just a single command. The HEAD pointer is reset to the specified commit but all the changes are tracked and staged still.

```
git reset --soft <commit>
git commit -m 'your commit message'
```

@annotation:tour uc-resetfile
#Resetting a file
You can also reset a single file : 

- `git reset <filename>` removes a file from the staging area but leaves the working directory unchanged.
- `git reset --hard <filename>` removes a file from the staging area *and* from the working directory.

@annotation:tour play
#Playtime
Use the current project to play around with the variations of `git reset`. 

Once you are fully comfortable, move on the next module which shows you a different, safer way of undoing changes.


